@echo off
:: ========================================================================
:: $File: build.bat $
:: $Date: 2023-09-18 19:03:32 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

set VCPKG_ROOT=Z:\Program_Files\vcpkg\

set PATH=%PATH%;%VCPKG_ROOT%
set PATH=%PATH%;Z:\Program_Files\cmake-3.27.4\bin\

cd ../

::echo ::Build ninja
::cmake -S . -B build/ninja/ -GNinja

::echo ::Gathering compilation database information!
::copy build\ninja\compile_commands.json .

echo ::Build MSVC solution
cmake -S . -B build/windows/

